package ch.teko.oop.pruefung.testing

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Assert.fail
import org.junit.Test


class TravelOfficeTest {

    @Test
    fun should_book_room_if_is_sunny() {
        val mockWeatherService : WeatherService = mock()

        whenever(mockWeatherService.getWeather()).thenReturn(Weather.SUNNY)

        val travelOffice = TravelOffice(mockWeatherService)

        val shouldBookRoom = travelOffice.shouldBookRoom()

        verify(mockWeatherService).getWeather()
        assertThat(shouldBookRoom, equalTo(true))

    }

    @Test
    fun should_book_room_if_is_cloudy() {
        //TODO Testfall implementieren
        fail()
    }

    @Test
    fun should_not_book_room_if_is_rainy() {
        //TODO Testfall implementieren
        fail()
    }


}