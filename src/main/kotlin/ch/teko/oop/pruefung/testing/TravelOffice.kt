package ch.teko.oop.pruefung.testing

class TravelOffice(val weatherService: WeatherService) {

    fun shouldBookRoom() : Boolean {
        val weather = weatherService.getWeather()

        if (weather == Weather.SUNNY) {
            return true
        } else if (weather == Weather.CLOUDY) {
            return true
        } else if (weather == Weather.CLOUDY) {
            return false
        }

        return false
    }

}