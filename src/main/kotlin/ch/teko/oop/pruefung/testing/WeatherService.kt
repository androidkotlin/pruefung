package ch.teko.oop.pruefung.testing

interface WeatherService {

    fun getWeather() : Weather

}

enum class Weather {
    SUNNY, CLOUDY, RAINY
}