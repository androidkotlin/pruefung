package ch.teko.oop.pruefung.rest.controller

import ch.teko.oop.pruefung.rest.model.Account
import ch.teko.oop.pruefung.rest.repository.AccountRepository
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestBody
import javax.validation.Valid

//TODO /account-api
class AccountController(val accountRepository: AccountRepository) {

    //TODO /accounts
    fun createAccount(/*TODO annotationen setzen*/ account: Account) : ResponseEntity<Account> {
        //TODO Bitte implementieren
        return ResponseEntity.ok().build()
    }

    //TODO /accounts
    fun getAllAccounts() : List<Account> {
        //TODO Bitte implementieren
        return arrayListOf()
    }

    //TODO /accounts/33
    fun getAccountById(/*TODO annotationen setzen*/ id:Long) : ResponseEntity<Account > {
        //TODO Bitte implementieren
        return ResponseEntity.ok().build()
    }

    //TODO /accounts/33
    fun updateAccountById(/*TODO annotationen setzen*/ account: Account, id:Long) : ResponseEntity<Any> {
        //TODO Bitte implementieren
        return ResponseEntity.ok().build()
    }

    //TODO /accounts/33
    fun deleteAccountById(/*TODO annotationen setzen*/ id:Long) : ResponseEntity<Any> {
        //TODO Bitte implementieren
        return ResponseEntity.ok().build()
    }



}