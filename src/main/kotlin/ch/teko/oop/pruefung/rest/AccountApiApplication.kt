package ch.teko.oop.pruefung.rest

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AccountApiApplication

fun main(args: Array<String>) {
    runApplication<AccountApiApplication>(*args)
}
