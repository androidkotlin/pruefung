package ch.teko.oop.pruefung.rest.repository

import ch.teko.oop.pruefung.rest.model.Account
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AccountRepository : JpaRepository<Account, Long>