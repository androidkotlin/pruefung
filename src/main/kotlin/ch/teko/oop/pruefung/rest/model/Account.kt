package ch.teko.oop.pruefung.rest.model

//TODO Hier richtige Annotation setzen
class Account (

        //TODO Hier richtige Annotation setzen
        val id:Long = 0,

        //TODO Hier richtige Annotation setzen
        val accountNr:String = "",

        // Hier kommt keine Annotation, es muss nichts angepasst werden
        val amount:Double = 0.0
)