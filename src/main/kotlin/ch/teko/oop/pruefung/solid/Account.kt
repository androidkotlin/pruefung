package ch.teko.oop.pruefung.solid

class Account(val type: Type) {

    var amount:Double = 0.0

    fun pay(value:Double) {
        if (Type.TRANSACTIONAL == type) {
            amount = amount + value - 1
        } else if (Type.SAVINGS == type) {
            amount = amount + value * 1.01
        }
    }

}

enum class Type{
    TRANSACTIONAL, SAVINGS
}